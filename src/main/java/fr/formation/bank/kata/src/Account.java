/**
 * 
 */
package fr.formation.bank.kata.src;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author saad
 *
 */
public class Account {

	private String name;
	private Float balance;
	private List<Operation> histoyList;
	
	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public Account(String pName) {
		this.name = pName;
		this.balance = 0f;
		this.histoyList = new ArrayList<Operation>();
	}

	/** Getters/Setters  */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Float getBalance() {
		return balance;
	}
	public void setBalance(Float balance) {
		this.balance = balance;
	}

	public List<Operation> getHistoyList() {
		return histoyList;
	}
	public void setHistoyList(List<Operation> histoyList) {
		this.histoyList = histoyList;
	}

	// Methodes
	public void makeDeposit(Float pAmount) {

		this.balance += pAmount;
		addOperationHistory(pAmount, Operation.TYPE.DEPOSIT);

	}

	public boolean makeWithdrawal(Float pAmount) {
		if (this.balance >= pAmount) {
			this.balance -= pAmount;
			addOperationHistory(pAmount, Operation.TYPE.WiThDRAWAL);

			return true;
		} else {
			return false;
		}
	}
	
	private void addOperationHistory(Float pAmount, Operation.TYPE pTypeOperation) {
		String lDate = sdf.format(Calendar.getInstance().getTime());
		histoyList.add(new Operation(pTypeOperation, lDate, pAmount, this.balance));
	}
	

}
