package fr.formation.bank.kata.src;

public class Operation {

	private TYPE type;
	private String date;
	private Float amount;
	private Float balance;
	
	
	public Operation(TYPE pType, String pDate, Float pAmount, Float pBalance) {
		super();
		this.type = pType;
		this.date = pDate;
		this.amount = pAmount;
		this.balance = pBalance;
	}
	
	// GETTERS & SETTERS
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Float getBalance() {
		return balance;
	}
	public void setBalance(Float balance) {
		this.balance = balance;
	}
	
	public enum TYPE {
		DEPOSIT, WiThDRAWAL;
	}
	// METHODES

	@Override
	public String toString() {
		return "Operation [type=" + type + ", date=" + date + ", amount=" + amount + ", balance=" + balance + "]";
	}
	
	
}
