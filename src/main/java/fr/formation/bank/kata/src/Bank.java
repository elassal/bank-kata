/**
 * 
 */
package fr.formation.bank.kata.src;

import java.util.HashMap;
import java.util.Map;

/**
 * @author saad
 *
 */
public class Bank {

	
	static Map< String, Account> accountMap = new HashMap<String, Account>();
	
	
	// create new aacount, put into the map, return the created account
	public static Account createAccount(String pName) {
		Account myAccount = new Account(pName);
		accountMap.put(pName, myAccount);
		return myAccount;
	}


	public static Account findAccount(String pName) {
		return accountMap.get(pName);
	}


	public static boolean makeDeopsit(String pName, Float pAmount) {
		Account myAccount = accountMap.get(pName);
		if(myAccount != null) {
			myAccount.makeDeposit(pAmount);
			return true;
		}
		return false;
	}


	public static boolean makeWithdrawal(String pName, Float pAmount) {
		Account myAccount = accountMap.get(pName);
		if (myAccount != null) {
			pAmount = (pAmount != null) ? pAmount : myAccount.getBalance();
			
			return myAccount.makeWithdrawal(pAmount);
		}
		return false;
	}


	public static boolean makeWithdrawalALL(String pName) {
		return makeWithdrawal(pName, null);
	}

}
