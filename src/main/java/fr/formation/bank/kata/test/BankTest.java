/**
 * 
 */
package fr.formation.bank.kata.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.formation.bank.kata.src.Account;
import fr.formation.bank.kata.src.Bank;

/**
 * @author saad
 *
 */
public class BankTest {
	/** US 1*/
	// OK
	@Test
	public void create_account() {
		String name = "Name_01";
		
		Account account_1 = Bank.createAccount(name);
		
		assertNotNull(account_1);
		assertEquals(name, account_1.getName());
	}
	
	// Ok
	@Test
	public void find_account() {
		String name = "Name_01";
		
		Bank.createAccount(name);
		Account account_1 = Bank.findAccount(name);
		
		assertNotNull(account_1);
		assertEquals(name, account_1.getName());
	}
	
	// Ko
	@Test
	public void find_account_2() {
		String name = "Name_01";
		//
		Account account_1 = Bank.findAccount(name);
		//
		assertNull(account_1);
	}
	
	@Test
	// Ok
	public void make_deposit() {
		String name = "Name_01";
		Float amount = new Float(100);
		//
		Bank.createAccount(name);
		Bank.makeDeopsit(name, amount);
		//
		Account account_1 = Bank.findAccount(name);// find to compare
		assertEquals(amount, account_1.getBalance());
	}
	
	@Test
	// Ko
	public void make_deposit_2() {
		String name = "Name_02";
		Float amount = new Float(100);
		//
		assertEquals(false, Bank.makeDeopsit(name, amount));
	}
	
	/** US 2 */
	@Test
	//OK
	public void make_withdrawal() {
		String name = "Name_01";
		Float amount = new Float(100);
		//
		Bank.createAccount(name);
		Bank.makeDeopsit(name, amount);//+100
		Bank.makeDeopsit(name, amount);//+100
		//
		assertEquals(true, Bank.makeWithdrawal(name, amount));//-100
	}
	
	//OK
	public void make_withdrawalALL() {
		String name = "Name_01";
		Float amount = new Float(100);
		//
		Bank.createAccount(name);
		Bank.makeDeopsit(name, amount);// +100
		Bank.makeDeopsit(name, amount);// +100
		//
		assertEquals(true, Bank.makeWithdrawalALL(name));// -200
	}
	
	@Test
	//Ko
	public void make_withdrawal_2() {
		String name = "Name_02";
		Float amount = new Float(100);
		//
		assertEquals(false, Bank.makeWithdrawal(name, amount));
	}
	
	/** US 3 */
	@Test
	public void seeHistory() {
		String name = "Name_01";
		Float amount = new Float(100);
		//
		Account myAccount = Bank.createAccount(name);
		Bank.makeDeopsit(name, amount);// +100
		Bank.makeDeopsit(name, amount);// +100
		Bank.makeWithdrawal(name, amount);// -100
		
		//
//		myAccount.getHistoyList().stream()
//		.forEach(System.out::println);
//		Operation [type=DEPOSIT, date=2020/10/26 11:44:08, amount=100.0, balance=100.0]
//		Operation [type=DEPOSIT, date=2020/10/26 11:44:08, amount=100.0, balance=200.0]
//		Operation [type=WiThDRAWAL, date=2020/10/26 11:44:08, amount=100.0, balance=100.0]
		
		assertEquals(myAccount.getHistoyList().size(), 3);
	}
	
}
